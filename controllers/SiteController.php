<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use app\models\Ciclista;
use app\models\Puerto;
use app\models\Etapa;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta1() {
        $dataProvider = new SqlDataProvider(['sql' =>'SELECT count(*) as total FROM ciclista',
        ]);
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT count(*) as total FROM ciclista",
        ]);
    }
    
    public function actionConsulta1a() {
        $dataProvider = new ActiveDataProvider([
        'query' => Ciclista::find()->select("count(*) as total"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT count(*) as total FROM ciclista",
        ]);
    }
    
    
    public function actionConsulta2() {
        $dataProvider = new SqlDataProvider(['sql' =>'SELECT COUNT(dorsal) as total FROM ciclista WHERE nomequipo = "banesto"',
        ]);
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(dorsal) FROM ciclista WHERE nomequipo = 'banesto'",
        ]);
    }
    
    public function actionConsulta2a() {
        $dataProvider = new ActiveDataProvider([
        'query' => Ciclista::find()->select("count(*) as total")-> where('nomequipo = "banesto"'),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(dorsal) FROM ciclista WHERE nomequipo = 'banesto'",
        ]);
    }
    
    
    public function actionConsulta3() {
        $dataProvider = new SqlDataProvider(['sql' =>'SELECT AVG(edad) as total FROM ciclista',
        ]);
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad) as total FROM ciclista;",
        ]);
    }
    
    public function actionConsulta3a() {
        $dataProvider = new ActiveDataProvider([
        'query' => Ciclista::find()->select("avg(edad) as total"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad) as total FROM ciclista",
        ]);
    }
    
    
    public function actionConsulta4() {
        $dataProvider = new SqlDataProvider(['sql' =>'SELECT AVG(edad) as total FROM ciclista WHERE nomequipo = "banesto"',
        ]);
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>"SELECT AVG(edad) as total FROM ciclista WHERE nomequipo = 'banesto'",
        ]);
    }
    
    public function actionConsulta4a() {
        $dataProvider = new ActiveDataProvider([
        'query' => Ciclista::find()->select("avg(edad) as total")-> where("nomequipo = 'banesto'"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>"SELECT AVG(edad) as total FROM ciclista WHERE nomequipo = 'banesto'",
        ]);
    }
    
    
    public function actionConsulta5() {
        $dataProvider = new SqlDataProvider(['sql' =>'SELECT AVG(edad) as total FROM ciclista GROUP BY nomequipo',
        ]);
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT AVG(edad) as total FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
    public function actionConsulta5a() {
        $dataProvider = new ActiveDataProvider([
        'query' => Ciclista::find()->select("avg(edad) as total")-> groupBy("nomequipo"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT AVG(edad) as total FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
    
    public function actionConsulta6() {
        $dataProvider = new SqlDataProvider(['sql' =>'SELECT COUNT(dorsal) as total FROM ciclista GROUP BY nomequipo',
        ]);
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT COUNT(dorsal) as total FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
    public function actionConsulta6a() {
        $dataProvider = new ActiveDataProvider([
        'query' => Ciclista::find()->select("count(dorsal) as total")-> groupBy("nomequipo"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT COUNT(dorsal) as total FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
    
    public function actionConsulta7() {
        $dataProvider = new SqlDataProvider(['sql' =>'SELECT COUNT(nompuerto) as total FROM puerto',
        ]);
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(nompuerto) as total FROM puerto",
        ]);
    }
    
    public function actionConsulta7a() {
        $dataProvider = new ActiveDataProvider([
        'query' => puerto::find()->select("count(nompuerto) as total"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(nompuerto) as total FROM puerto",
        ]);
    }
    
    
    public function actionConsulta8() {
        $dataProvider = new SqlDataProvider(['sql' =>'SELECT COUNT(nompuerto) as total FROM puerto WHERE altura>1500',
        ]);
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(nompuerto) as total FROM puerto WHERE altura>1500",
        ]);
    }
    
    public function actionConsulta8a() {
        $dataProvider = new ActiveDataProvider([
        'query' => puerto::find()->select("count(nompuerto) as total")->where("altura>1500"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(nompuerto) as total FROM puerto WHERE altura>1500",
        ]);
    }
    
    
    public function actionConsulta9() {
        $dataProvider = new SqlDataProvider(['sql' =>'SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(dorsal)>4',
        ]);
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(dorsal)>4",
        ]);
    }
    
    public function actionConsulta9a() {
        $dataProvider = new ActiveDataProvider([
        'query' => Ciclista::find()->select("nomequipo")-> groupBy("nomequipo")-> having("count(dorsal)>4"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(dorsal)>4",
        ]);
    }
    
    
    public function actionConsulta10() {
        $dataProvider = new SqlDataProvider(['sql' =>'SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(dorsal)>4',
        ]);
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(dorsal)>4",
        ]);
    }
    
    public function actionConsulta10a() {
        $dataProvider = new ActiveDataProvider([
        'query' => Ciclista::find()->select("nomequipo")->where("edad between 28 and 32")-> groupBy("nomequipo")-> having("count(dorsal)>4"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(dorsal)>4",
        ]);
    }
    
    
    public function actionConsulta11() {
        $dataProvider = new SqlDataProvider(['sql' =>'SELECT dorsal,COUNT(numetapa) as total FROM etapa GROUP BY dorsal',
        ]);
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','total'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT dorsal,COUNT(numetapa) as total FROM etapa GROUP BY dorsal",
        ]);
    }
    
    public function actionConsulta11a() {
        $dataProvider = new ActiveDataProvider([
        'query' => etapa::find()->select("dorsal, COUNT(numetapa) as total")-> groupBy("dorsal"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','total'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT dorsal,COUNT(numetapa) as total FROM etapa GROUP BY dorsal",
        ]);
    }
    
    
    public function actionConsulta12() {
        $dataProvider = new SqlDataProvider(['sql' =>'SELECT dorsal,COUNT(numetapa) as total FROM etapa GROUP BY dorsal HAVING COUNT(numetapa)>1',
        ]);
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','total'],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal,COUNT(numetapa) as total FROM etapa GROUP BY dorsal HAVING COUNT(numetapa)>1",
        ]);
    }
    
    public function actionConsulta12a() {
        $dataProvider = new ActiveDataProvider([
        'query' => etapa::find()->select("dorsal, COUNT(numetapa) as total")-> groupBy("dorsal")-> having("count(numetapa)>1"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','total'],
            "titulo"=>"Consulta 12 con Active Record",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal,COUNT(numetapa) as total FROM etapa GROUP BY dorsal HAVING COUNT(numetapa)>1",
        ]);
    }


}
